package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public interface DataBaseDataSource {
    List<Persona> obtainItems () throws BaseDataItemsException;
}