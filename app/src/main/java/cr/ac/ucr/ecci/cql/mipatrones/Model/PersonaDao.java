package cr.ac.ucr.ecci.cql.mipatrones.Model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonaDao {
    @Query("SELECT * FROM  persona")
    List<Persona> getAll();

    @Query("SELECT * FROM  persona WHERE identificacion IN (:identificaciones)")
    List<Persona> loadAllByIds(String[] identificaciones);

    @Query("SELECT * FROM Persona WHERE nombre LIKE :name LIMIT 1")
    Persona findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Persona... personas);

    @Delete
    void delete(Persona persona);
}
