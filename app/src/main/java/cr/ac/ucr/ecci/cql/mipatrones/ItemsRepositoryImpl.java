package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valores
public class ItemsRepositoryImpl implements ItemsRepository {
    private DataBaseDataSource mDataBaseDataSource;
    @Override
    public List<Persona> obtainItems() throws CantRetrieveItemsException {
        List<Persona> items = null;
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        // Ejemplo: base de datos local, archivos locales, archivos en la red, bases de datos en la red
        try {
            mDataBaseDataSource = new DataBaseDataSourceImpl();
            items = mDataBaseDataSource.obtainItems();
        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
        return items;
    }
}