package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de presentacion (Vista)
// Implementado por MainActivity
public interface MainActivityView {
    // Mostrar el progreso en la UI del avance de la tarea a realizar
    void showProgress();
    // Esconder el indicador de progreso de la UI
    void hideProgress();
    // Mostrar los items de la lista en la UI
    void setItems(List<Persona> items);
    // Mostrar mensaje en la UI
    void showMessage(String message);
    //Muestra el fragmento
    void setFragment(Persona item);
}