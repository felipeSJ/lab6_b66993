package cr.ac.ucr.ecci.cql.mipatrones.Model;

import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;

import cr.ac.ucr.ecci.cql.mipatrones.Model.AppDataBase;

public class DeploymentScript {
    private AppDataBase db;

    public AppDataBase runScript(Context context){
        db = Room.databaseBuilder(context, AppDataBase.class, "database").allowMainThreadQueries().build();
        crearPersonas();
//        db.close();
        return db;
    }

    private void crearPersonas(){

        db.personaDao().insertAll(
                new Persona("B66993","Felipe Solís", "i01.png", "Computación e informática",
                        23, "61368715", "felipesj@hotmail.com"),
                new Persona("A54321", "Paco Hernández", "i01.png", "Odontología",
                        28, "87323456", "ph@hotmail.com"),
                new Persona("A12345", "Lola Jiménez", "i01.png", "Filosofía",
                        25, "67891234", "LolaJim@hotmail.com"),
                new Persona("A90871", "Juan Boza", "i01.png", "Geografía",
                        23, "86547901", "bozaJuan@hotmail.com"),
                new Persona("B90871", "Valeria Aguilar", "i01.png", "Medicina",
                        23, "83534700", "valagui@hotmail.com")
        );
    }
}
