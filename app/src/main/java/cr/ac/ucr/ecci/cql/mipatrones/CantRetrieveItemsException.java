package cr.ac.ucr.ecci.cql.mipatrones;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
