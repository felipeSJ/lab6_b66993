package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

public class LazyAdapter extends BaseAdapter {
    private List<Persona> mData;
    private Context mContext;

    public LazyAdapter(List<Persona> data, Context context) {
        mData = data;
        mContext = context;
    }

    public int getCount() {
        if(mData != null){
            return mData.size();
        }
        return 0;
    }


    public Persona getItem(int position) {
        return mData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;
        Persona mPersona = mData.get(position);
        rowView = inflater.inflate(R.layout.list_row, parent, false);
        TextView nombre = (TextView)rowView.findViewById(R.id.nombre);
        TextView identificacion = (TextView)rowView.findViewById(R.id.identificacion);
        ImageView imagen = (ImageView)rowView.findViewById(R.id.imagen);
        nombre.setText(mPersona.getNombre());
        identificacion.setText(mPersona.getIdentificacion());
        imagen.setImageResource(R.drawable.i01);
        return rowView;
    }
}

