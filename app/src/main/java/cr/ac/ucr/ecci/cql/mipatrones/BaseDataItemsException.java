package cr.ac.ucr.ecci.cql.mipatrones;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}