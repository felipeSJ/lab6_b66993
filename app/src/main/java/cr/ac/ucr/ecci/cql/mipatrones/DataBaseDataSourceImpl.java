package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import cr.ac.ucr.ecci.cql.mipatrones.Model.AppDataBase;
import cr.ac.ucr.ecci.cql.mipatrones.Model.DeploymentScript;
import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {


        List<Persona> items = null;
        try {
            // TODO: Obtener de la base de datos
//            AppDataBase db = Room.databaseBuilder(MainActivity.getContext(), AppDataBase.class, "database-name").allowMainThreadQueries().build();
            AppDataBase db = MainActivity.getDb();
            items = createArrayList(db);
            db.close();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }

    // Esta lista debe recuperarla de la base de datos
    // para el ejemplo la inicializamos con datos dummy
//    private List<String> createArrayList() {
//        return Arrays.asList(
//                "Soda 1",
//                "Soda 2",
//                "Soda 3",
//                "Soda 4",
//                "Soda 5",
//                "Soda 6",
//                "Soda 7",
//                "Soda 8",
//                "Soda 9",
//                "Soda 10"
//        );
//    }

    private List<Persona> createArrayList(final AppDataBase db) {
        ArrayList<Persona> personas = new ArrayList<>();
        for (Persona p: db.personaDao().getAll()) {
            personas.add(p);
        }
        return personas.subList(0, personas.size());
    }
}