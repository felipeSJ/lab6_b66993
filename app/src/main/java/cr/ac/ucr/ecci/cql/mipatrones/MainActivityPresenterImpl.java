package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de presentacion (Presenter)
// Presenter (P) -> Implementacion de MainActivityPresenter y de GetListItemsInteractor.OnFinishedListener
public class MainActivityPresenterImpl implements MainActivityPresenter, GetListItemsInteractor.OnFinishedListener{

    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView){
        this.mMainActivityView=mainActivityView;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor=new GetListItemsInteractorImpl();
    }

    @Override
    public void onResume(){
        if(mMainActivityView!=null){
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this);
    }

    // Evento de clic en la lista
    @Override
    public void onItemClicked(Persona item){
        if(mMainActivityView!=null){
            mMainActivityView.setFragment(item);
        }
    }

    @Override
    public void onDestroy(){
        mMainActivityView=null;
    }

    @Override
    public void onFinished(List<Persona> items){
        if(mMainActivityView!=null){
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }
    // Retornar la vista
    public MainActivityView getMainView(){
            return mMainActivityView;
    }
}
