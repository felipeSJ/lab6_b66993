package cr.ac.ucr.ecci.cql.mipatrones;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de presentacion (Presenter)
// Presenter (P) -> Interface de la capa Presenter implementado por MainActivityPresenterImpl
public interface MainActivityPresenter {
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(Persona item);
    // destruir
    void onDestroy();
}
