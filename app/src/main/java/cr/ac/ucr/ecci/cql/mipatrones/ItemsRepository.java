package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public interface ItemsRepository {
    List<Persona> obtainItems() throws CantRetrieveItemsException;
}