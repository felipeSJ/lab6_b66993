package cr.ac.ucr.ecci.cql.mipatrones;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cr.ac.ucr.ecci.cql.mipatrones.Model.Persona;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonaFragment extends Fragment {

    private Persona item;

    public PersonaFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public static PersonaFragment newInstance(int index) {
        PersonaFragment f = new PersonaFragment();
        // Provee el index como argumento para mostrar el detalle de datos.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            this.item = bundle.getParcelable("currentItem");
        }
        return inflater.inflate(R.layout.fragment_persona, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.setTextValues(view);
    }

    public void setTextValues(View view){
        TextView nombre = view.findViewById(R.id.itemNombre);
        nombre.setText(item.getNombre());

        TextView carrera = view.findViewById(R.id.itemCarrera);
        carrera.setText(item.getCarrera());

        TextView edad = view.findViewById(R.id.itemEdad);
        edad.setText(Integer.toString(item.getEdad()));

        TextView celular = view.findViewById(R.id.itemCelular);
        celular.setText(item.getCelular());

        TextView correo = view.findViewById(R.id.itemCorreo);
        correo.setText(item.getCorreo());
    }
}