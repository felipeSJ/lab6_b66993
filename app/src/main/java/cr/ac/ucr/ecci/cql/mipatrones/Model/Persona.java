package cr.ac.ucr.ecci.cql.mipatrones.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Persona implements Parcelable {
    @PrimaryKey @NonNull
    private String identificacion;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "id_Imagen")
    private String idImagen;

    @ColumnInfo(name = "carrera")
    private String carrera;

    @ColumnInfo(name = "edad")
    private int edad;

    @ColumnInfo(name = "celular")
    private String celular;

    @ColumnInfo(name = "correo")
    private String correo;

    public Persona(){}

    public Persona(String identificacion, String nombre, String idImagen, String carrera, int edad, String celular, String correo){
        setIdentificacion(identificacion);
        setNombre(nombre);
        setIdImagen(idImagen);
        setCarrera(carrera);
        setEdad(edad);
        setCelular(celular);
        setCorreo(correo);
    }

    protected Persona(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
        idImagen = in.readString();
        carrera = in.readString();
        edad = in.readInt();
        celular = in.readString();
        correo = in.readString();
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(String idImagen) {
        this.idImagen = idImagen;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

//    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
//        @Override
//        public Persona createFromParcel(Parcel in) {
//            return new Persona(in);
//        }
//        @Override
//        public Persona[] newArray(int size) {
//            return new Persona[size];
//        }
//    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeString(idImagen);
        dest.writeString(carrera);
        dest.writeInt(edad);
        dest.writeString(celular);
        dest.writeString(correo);
    }

}
