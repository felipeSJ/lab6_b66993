package cr.ac.ucr.ecci.cql.mipatrones.Model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Persona.class}, version = 2)
public abstract class AppDataBase extends RoomDatabase {
    public abstract PersonaDao personaDao();
}
